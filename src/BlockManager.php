<?php


namespace NoTee;


class BlockManager implements BlockManagerInterface
{
    protected array $blocks = [];
    protected array $extends = [];


    public function define(string $name, callable $callable): void
    {
        if (isset($this->blocks[$name])) {
            throw new \InvalidArgumentException("Block '$name' is already defined");
        }
        $this->blocks[$name] = $callable;
    }

    public function extend(string $name, callable $callable): void
    {
        if (!isset($this->extends[$name])) {
            $this->extends[$name] = [$callable];
        } else {
            $this->extends[$name][] = $callable;
        }
    }

    public function compose(string $name): NodeInterface
    {
        if (!isset($this->blocks[$name])) {
            throw new \InvalidArgumentException("Block '$name' is not defined");
        }
        $callables = array_merge([$this->blocks[$name]], array_reverse($this->extends[$name] ?? []));
        return $this->composeRecursive($name, $callables);
    }

    protected function composeRecursive(string $name, array $callables)
    {
        $callable = array_pop($callables);
        assert(is_callable($callable));

        /*
         * A block callable can call the parent implementation, but it does not have to do so. Therefore we provide the
         * parent call as another callable that provides the result of the parent. The cool thing here is, that this
         * the parent block implementation is lazy loaded.
         */
        return $callable(function() use ($name, $callables) {
            return $this->composeRecursive($name, $callables);
        });
    }
}