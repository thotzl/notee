<?php


namespace NoTee;


class Template implements TemplateInterface
{
    protected AbstractNodeFactory $nodeFactory;
    protected array $templateDirs;
    protected array $defaultContext;

    /**
     * Template constructor.
     * @param array $templateDirs
     * @param AbstractNodeFactory $nodeFactory
     * @param array $defaultContext Variables provided in this array are added to the context in all render calls.
     */
    public function __construct(array $templateDirs, AbstractNodeFactory $nodeFactory, array $defaultContext = [])
    {
        $this->templateDirs = $templateDirs;
        $this->nodeFactory = $nodeFactory;
        $this->defaultContext = $defaultContext;
    }

    /**
     * @param string $template
     * @param array $context
     * @return NodeInterface
     * @throws ContextConflictException
     */
    public function render(string $template, array $context = []): NodeInterface
    {
        $contextIntersect = array_intersect(array_keys($this->defaultContext), array_keys($context));
        if (!empty($contextIntersect)) {
            $intersectString = implode(',', $contextIntersect);
            throw new ContextConflictException("Context variables are conflicting with global context variables: $intersectString");
        }
        $context = array_merge($this->defaultContext, $context);
        $template = trim($template, '/\\');
        $dirs = array_reverse($this->templateDirs);
        foreach ($dirs as $dir) {
            $path = $dir . '/' . $template;
            if (file_exists($path)) {
                $return = Scoping::requireScoped($path, $context);
                if ($return === null) {
                    return $this->nodeFactory->wrapper();
                } elseif ($return !== 1) {
                    return $return;
                }
            }
        }
        throw new \InvalidArgumentException("Template File '$template' either does not exist or does not return an instance of " . NodeInterface::class);
    }
}