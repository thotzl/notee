<?php

namespace NoTee;


interface EscapingStrategyInterface
{
    public function escapeHtml(string $value) : string;
    public function escapeAttribute(string $value) : string;
}
