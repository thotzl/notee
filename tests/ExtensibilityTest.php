<?php


namespace NoTee;


use PHPUnit\Framework\TestCase;

class ExtensibilityTest extends TestCase
{
    public function test()
    {
        global $noTee;
        $noTee = new NodeFactory(new DefaultEscapingStrategy('utf-8'), new UriValidator(), new BlockManager());
        require_once __DIR__ . '/../global.php';
        $templateDirs = [
            __DIR__ . '/example_project/base',
            __DIR__ . '/example_project/first_child',
            __DIR__ . '/example_project/last_child',
        ];
        $template = new Template($templateDirs, $noTee);
        $noTee->setTemplate($template);
        $node = $template->render('test.html.php', ['titleExtension' => ' is cool']);
        $this->assertEquals(
            (string)_document(
                _html(
                    _head(
                        _title('NoTee is cool')
                    ),
                    _body(
                        'Hello Hello World World World'
                    ),
                )
            ),
            (string)$node
        );
    }

    public function testContextConflictException()
    {
        $nf = new NodeFactory(new DefaultEscapingStrategy('utf-8'), new UriValidator(), new BlockManager());
        $template = new Template([], $nf, [
            'var1' => 1,
            'var2' => 2,
            'var3' => 3,
        ]);
        $this->expectException(ContextConflictException::class);
        $template->render('irrelevant.php', [
            'var2' => 1,
        ]);
    }
}