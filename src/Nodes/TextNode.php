<?php

declare(strict_types=1);

namespace NoTee\Nodes;

use NoTee\EscapingStrategyInterface;
use NoTee\NodeInterface;

class TextNode implements NodeInterface
{
    protected string $text;

    public function __construct(string $text, EscapingStrategyInterface $escaper)
    {
        $this->text = $escaper->escapeHtml($text);
    }

    public function __toString(): string
    {
        return $this->text;
    }

}
