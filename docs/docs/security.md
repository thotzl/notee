NoTee was designed with security in mind. Depending on the use case, NoTee is more secure than traditional template
engines like Twig or Smarty. In other use cases, NoTee is the wrong choice and should not be used.

This guide provides security considerations.

## When to use NoTee

When using NoTee, you are using plain PHP. This gives you a lot of power. But with great power comes great
responsibility. Traditional template engines can be restricted, so that they can be used by users of the software
(e.g. email templates in an administration area). This is not possible with NoTee. Therefore you should only use NoTee
for code, that is produces by developers in a development environment.

## Escaping Strategy and Encoding

When creating an instance of the NodeFactory class, you need to provide an object of type
`NoTee\EscapingStrategyInterface`. NoTee provides a default implementation under `NoTee\DefaultEscapingStrategy`. It is
recommended to use the default implementation. If you provide your own implementation you should be absolutely sure
about what you are doing.

The class `NoTee\DefaultEscapingStrategy` expects, that you provide the correct encoding. You should declare the same
encoding as you define in your http header and/or html meta charset declaration. If you declare another encoding, this
could lead to security vulnerabilities in your application.

**Using the right charset is essential for the security of your app!!! Please make sure, that your response (header and/or body) contains the correct charset declaration!**

## NoTee is more secure

NoTee is more secure, compared to traditional template engines, because it understands the semantic of the generated
HTML. E.g. NoTee makes sure, that URI's do not contain malicious code by checking the URI scheme.

You cannot have an uri with the scheme `javascript:` when using NoTee. This level of security is not provided by any
other library or template engine that I am aware of.

## Recommendations

In theory, you should not have any XSS vulnerabilities in your application, if you are using NoTee. Nevertheless I would
recommend, that you add an CSP to your application. Most applications have to allow user generated html somewhere.
In those cases you need to output HTML with the `raw` function. This function cannot protect your app from XSS, because
it needs to output plain html.

Therefore you should add a CSP, just to be absolutely sure.